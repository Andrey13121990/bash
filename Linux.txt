
-------------------------- Начало --------------------------

whoami
groups
id
env
hostname
pwd
uname
who / w		// sessions



-------------------------- Данные --------------------------

df		    // hard disc
df -i 		// inodes (metadata)



-------------------------- Память --------------------------

free		//
free -h		// in Gb (human readable)



-------------------------- Навигация --------------------------

ls
ls -l		// show full data
ls -a 		// show all including hidden
ls -al		// show full and all
mc 		    // midnight commander
cd		    // change directory



-------------------------- Поиск --------------------------

ls | grep	// поиск внутри файлов (перенаправление из ls)
find
locate



-------------------------- Просмотр  --------------------------

cat 
cat | less
less
more
vim		    // текстовый редактор



-------------------------- Создание файлов --------------------------

cat > file
touch file


-------------------------- Программы --------------------------

echo $PATH
ps ax			        // process
grep p			        // search
ps ax | grep process	// search in process list
bg			            // продолжить выполнение в фоне
fg			            // return program after ctrl+z
jobs			        // фоновые задачи



---------------------------- Постгрес --------------------------

sudo -u postgres psql
\connect postgres